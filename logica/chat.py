import random
import json
import torch
import pandas as pd

from utils.csv_tools import * 
from logica.model import NeuralNet
from utils.nltk_tools import bag_of_words, tokenize, corregir_ortografia
from nltk.corpus import stopwords

stopwords_espanol = set(stopwords.words('spanish'))

def conversacion(text):
    text = corregir_ortografia(text)
    path_file = "archivos/COMISIONES-DE-UN-PERIODO-1_2024-ANUAL_2024-v10-CON-AULAS - Table 1.csv"
    df = pd.read_csv(path_file, encoding='utf-8')
    original_text = text

    with open('json/materias.json', 'r', encoding="utf-8") as f:
        materias = json.load(f)

    device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')

    with open('json/intents.json', 'r', encoding="utf-8") as json_data:
        intents = json.load(json_data)

    FILE = "entrenamiento/data.pth"
    data = torch.load(FILE)
    input_size = data["input_size"]
    hidden_size = data["hidden_size"]
    output_size = data["output_size"]
    all_words = data['all_words']
    tags = data['tags']
    model_state = data["model_state"]

    model = NeuralNet(input_size, hidden_size, output_size).to(device)
    model.load_state_dict(model_state)
    model.eval()

    text = tokenize(text)

    X = bag_of_words(text, all_words)
    X = X.reshape(1, X.shape[0])
    X = torch.from_numpy(X).to(device)
    output = model(X)

    _, predicted = torch.max(output, dim=1)
    tag = tags[predicted.item()]

    probs = torch.softmax(output, dim=1)
    prob = probs[0][predicted.item()]

    if prob.item() > 0.75:
        print(10)
        for intent in intents['intenciones']:
            if tag == intent["tag"]:
                chain = ""
                print(tag)
                if tag == 'Comisión':
                    chain += filtrar_comision(df,original_text,materias)
                if tag == 'Horarios':
                    chain += filtrar_horarios(df,original_text,materias)
                if tag == 'Dias':
                    chain += filtrar_dia(df,original_text,materias)
                if tag == 'Aula':
                    chain += filtrar_aula(df,original_text,materias)
                return random.choice(intent['responses'])+"\n\n"+chain
    else:
        return "No entiendo, sé más claro"
