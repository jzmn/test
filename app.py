from typing import Final
from telegram import Update
from telegram.ext import Application, CommandHandler, MessageHandler, filters, ContextTypes
from logica.chat import conversacion

TOKEN: Final = '7018786012:AAHA7CaFO3jmHiWeIR_XuuEp6S5FYOI6kFI'

async def start_command(update: Update, context: ContextTypes.DEFAULT_TYPE):
    await update.message.reply_text('Hola, soy  unibot y estoy aquí para responder tus preguntas')


async def help_command(update: Update, context: ContextTypes.DEFAULT_TYPE):
    await update.message.reply_text('Por favor, escribe algo en lo que pueda ayudarte')

async def help_horario(update: Update, context: ContextTypes.DEFAULT_TYPE):
    await update.message.reply_text('escribe la materia que buscas (case sensitive)')

def respuesta(text: str) -> str:
    return conversacion(text)


async def error(update: Update, context: ContextTypes.DEFAULT_TYPE):
    print(f'Update {update} caused error {context}')


async def handle_message(update: Update, context: ContextTypes.DEFAULT_TYPE):
    message_type: str = update.message.chat.type
    text: str = update.message.text

    response: str = respuesta(text)
    await update.message.reply_text(response)


if __name__ == '__main__':

    app = Application.builder().token(TOKEN).build()
    print('starting bot')
    app.add_handler(CommandHandler('start', start_command))
    app.add_handler(CommandHandler('help', help_command))

    app.add_handler(MessageHandler(filters.TEXT, handle_message))

    app.add_error_handler(error)
    print('polling...')
    app.run_polling(poll_interval=3)







