import numpy as np
import nltk
from nltk.stem.snowball import SnowballStemmer
import requests

# nltk.download('punkt')

stemmer = SnowballStemmer('spanish')


def tokenize(sentence):
    return nltk.word_tokenize(sentence, language='spanish')


def stem(word):
    return stemmer.stem(word.lower())


def bag_of_words(tokenized_sentence, words):
    # stemming de cada palabra
    sentence_words = [stem(word) for word in tokenized_sentence]
    # Inicializar bag of words
    bag = np.zeros(len(words), dtype=np.float32)
    for idx, w in enumerate(words):
        if w in sentence_words:
            bag[idx] = 1
    return bag


def corregir_ortografia(frase):
    url = "https://api.languagetool.org/v2/check"
    params = {
        'text': frase,
        'language': 'es',
    }
    response = requests.get(url, params=params)
    data = response.json()
    palabras_excluidas = ['autocomprensión', 'con']

    if 'matches' in data and data['matches']:
        # Si hay errores, corregirlos automáticamente
        for match in reversed(data['matches']):  # Reversed para evitar problemas con los índices al corregir
            offset = match['offset']
            length = match['length']
            correction = match['replacements'][0]['value']  # Tomar la primera sugerencia de corrección
            if palabras_excluidas and correction.lower() in palabras_excluidas:
                continue  # Ignorar correcciones para palabras excluidas
            frase = frase[:offset] + correction + frase[offset + length:]
    return frase
