import pandas as pd
import re
import nltk
from utils.nltk_tools import tokenize, bag_of_words, stem
from nltk.corpus import stopwords

# nltk.download('stopwords')
stopwords_espanol = set(stopwords.words('spanish'))


def filtrar_horarios(df, original_text, materias_json):
    mat = buscar_materia(original_text, materias_json)
    com = buscar_comision(original_text)
    dia = buscar_fecha(original_text)

    if mat == "":
        return "Por favor, especifica una materia"
    else:
        materias_filtradas = df[df['Actividad'] == mat.upper()]
        if materias_filtradas.empty: return "Por favor, especifica una materia válida"
        if dia != "":
            dias_filtrados = df[df['Día'].str.lower() == dia.lower()]
            diasxmaterias = pd.merge(materias_filtradas, dias_filtrados, how='inner')
            if diasxmaterias.empty: return "No hay clases de " + mat + " los " + dia
            if com == "":
                return filtrar_comxhorario_str(diasxmaterias)
            else:
                comisiones_filtradas = df[df['Comisión'].str.contains("COM-" + com)]
                comisionesxmaterias = pd.merge(materias_filtradas, comisiones_filtradas, how='inner')

                if comisionesxmaterias.empty: return "comisión inexistente: " + com
                resultado_final = comisionesxmaterias[comisionesxmaterias['Día'].str.lower() == dia.lower()]

                if resultado_final.empty: return "No hay clases en ese día para la comisión " + com
                return filtrar_horarioxdia_str(resultado_final)
        else:
            if com != "":
                comisiones_filtradas = df[df['Comisión'].str.contains("COM-" + com)]
                comisionesxmaterias = pd.merge(materias_filtradas, comisiones_filtradas, how='inner')
                if comisionesxmaterias.empty: return "No hay una comisión y materia asociadas a las que mencionaste " + com
                return filtrar_horarioxdia_str(comisionesxmaterias)
            else:
                return filtrar_comxhorarioxdia_str(materias_filtradas)


def filtrar_comision(df, original_text, materias_json):
    mat = buscar_materia(original_text, materias_json)
    com = buscar_comision(original_text)

    if com != "":
        comisiones_filtradas = df[df['Comisión'].str.contains("COM-" + com)]
        if comisiones_filtradas.empty:
            return "Comisión inexistente " + com
        else:
            if mat != "":
                materias_filtradas = df[df['Actividad'] == mat.upper()]
                if materias_filtradas.empty: return "Por favor, especifica una materia válida"
                comisionesxmaterias = pd.merge(materias_filtradas, comisiones_filtradas, how='inner')
                return str(comisionesxmaterias[['Actividad', 'Comisión', 'Día', 'Horario']])
            else:
                return "Por favor especifica una materia"
    else:
        if mat != "":
            materias_filtradas = df[df['Actividad'] == mat.upper()]
            if materias_filtradas.empty: return "Por favor, especifica una materia válida"
            return str(materias_filtradas[['Actividad', 'Comisión', 'Día', 'Horario']])
        else:
            return "Por favor, ingresa una comisión o una materia valida"


def filtrar_aula(df, original_text, materias_json):
    mat = buscar_materia(original_text, materias_json)
    com = buscar_comision(original_text)

    if mat == "":
        return "Por favor, especifica una materia"
    else:
        materias_filtradas = df[df['Actividad'] == mat.upper()]
        if materias_filtradas.empty:
            return "Por favor, especifica una materia válida"
        else:
            if com != "":
                com_filtrada = df[df['Comisión'].str.contains("COM-" + com)]
                if com_filtrada.empty: return "Por favor, revisa la comisión nuevamente"
                comisionesxmaterias = pd.merge(materias_filtradas, com_filtrada, how='inner')
                if comisionesxmaterias.empty:
                    return "No hay una comisión y materia asociadas a las que mencionaste " + com
                else:
                    return filtrar_aula_str(comisionesxmaterias)
            else:
                return filtrar_aula_str(materias_filtradas)


def filtrar_dia(df, original_text, materias_json):
    mat = buscar_materia(original_text, materias_json)
    com = buscar_comision(original_text)
    if mat == "":
        return "Por favor, especifica una materia"
    else:
        materias_filtradas = df[df['Actividad'] == mat.upper()]
        if materias_filtradas.empty:
            return "Por favor, especifica una materia válida"
        if com == "":
            return "Por favor, ingresa una comisión"
        else:
            comision_filtrada = df[df['Comisión'].str.contains("COM-" + com)]
            if comision_filtrada.empty:
                return "Disculpa, no he podido encontrar la comisión registrada"
            else:
                comisionxmateria = pd.merge(materias_filtradas, comision_filtrada, how='inner')
                if comisionxmateria.empty:
                    return "Disculpa, la materia y la comisión mencionada no están registradas"
                else:
                    return filtrar_dia_str(comisionxmateria)


def buscar_fecha(text):
    dias_semana = ["lunes", "martes", "miércoles", "jueves", "viernes", "sábado"]
    patron = r"\b(?:{})\b".format("|".join(dias_semana))
    fecha_encontrada = re.findall(patron, text.lower(), flags=re.IGNORECASE)

    if len(fecha_encontrada) > 0:
        return fecha_encontrada[0]
    else:
        return ""


def buscar_aula(text):
    numero = r"\b\d{4}\b"
    if len(re.findall(numero, text)) > 0:
        return re.findall(numero, text)[0]
    else:
        return ""


def buscar_comision(text):
    text = text.lower()
    patron_comision = r"comisión \d+"
    patron_comxx = r"com[- ]\d+"
    numero = r"\d+"

    coincidencias_comision = re.findall(patron_comision, text)
    patron_comxx = re.findall(patron_comxx, text)

    if len(coincidencias_comision) > 0:
        print(re.findall(numero, coincidencias_comision[0])[0])
        return re.findall(numero, coincidencias_comision[0])[0]
    if len(patron_comxx) > 0:
        print(re.findall(numero, patron_comxx[0])[0])
        return re.findall(numero, patron_comxx[0])[0]
    else:
        return ""


def buscar_materia(text, materia_json):
    original_text = text
    text = tokenize(text.lower())
    text = [palabra for palabra in text if palabra.lower() not in stopwords_espanol]

    materia_encontrada = []
    for materia in materia_json['materias']:
        palabras_materia = tokenize(materia.lower())
        ignore_words = ['?', '.', '!']
        palabras_materia = [stem(w) for w in palabras_materia if w not in ignore_words]
        palabras_materia = [palabra for palabra in palabras_materia if palabra.lower() not in stopwords_espanol]

        bow = bag_of_words(text, palabras_materia)
        if (int(sum(bow)) == len(bow)):
            materia_encontrada.append(materia)

    for mat in materia_encontrada:
        # str(mat)
        if (str(mat).lower() not in original_text.lower()):
            materia_encontrada.remove(mat)
            print(materia_encontrada)
    if (len(materia_encontrada) == 0):
        return ""
    else:
        return str(materia_encontrada[0])


def filtrar_dia_str(df):
    formatted_string = ""
    for index, row in df.iterrows():
        formatted_string += f"{row['Día']}: {row['Actividad']} {row['Comisión']} \n"
    return formatted_string.upper()

def filtrar_aula_str(df):
    formatted_string = ""
    for index, row in df.iterrows():
        formatted_string += f"{row['Actividad']}: {row['Comisión']} en el {row['AULA']} \n\n"
    return formatted_string.upper()

def filtrar_horarioxdia_str(df):
    formatted_string = ""
    for index, row in df.iterrows():
        formatted_string += f"{row['Actividad']}: {row['Horario']} los {row['Día']} \n\n"
    return formatted_string.upper()

def filtrar_comxhorario_str(df):
    formatted_string = ""
    for index, row in df.iterrows():
        formatted_string += f"{row['Actividad']}: {row['Comisión']} a las {row['Horario']} \n\n"
    return formatted_string.upper()

def filtrar_comxhorarioxdia_str(df):
    formatted_string = ""
    for index, row in df.iterrows():
        formatted_string += f"{row['Actividad']}: {row['Comisión']} los {row['Día']} a las {row['Horario']} \n\n"
    return formatted_string.upper()